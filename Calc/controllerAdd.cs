using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace Calculator
{
    internal class controllerAdd
    { 
 
        public double add(double n1, double n2)
        {
            return (n1 + n2);
        }

        public void writeResult(double input)
        {
            Console.WriteLine("El resultado es:{0}\n", input);
        }

        public void doAdd(double n1, double n2)
        {
            double result = add(n1, n2);
            writeResult(result);
        }
    }
}