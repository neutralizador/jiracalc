﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;



namespace Calculator

{

    public partial class Calc : Form
    {

        controllerAdd contSum = new controllerAdd();
        controllerSub contSub = new controllerSub();
        controllerMul contMul = new controllerMul();
        controllerDiv contDiv = new controllerDiv();
        
        public Calc()
        {
            InitializeComponent();
        }

        private void addNum(string num)
        {
            if (txtpantalla.Text.Contains("="))
                txtpantalla.Text = "";
            if (txtpantalla.Text != "")
            {
                string[] nums = txtpantalla.Text.Split('+', '-', '*', '/');
                string last = nums[nums.Length - 1];
                if (last.Contains(".")) {
                    var d = last.Length - last.IndexOf(".");
                    if (d < 9)
                        txtpantalla.Text = txtpantalla.Text + num;
                } else
                    txtpantalla.Text = txtpantalla.Text + num;
            }
            else
                txtpantalla.Text = num;
        }

        private void btn1_Click(object sender, EventArgs e)
        {
            addNum("1");
        }

        private void btn2_Click(object sender, EventArgs e)
        {
            addNum("2");
        }

        private void btn3_Click(object sender, EventArgs e)
        {
            addNum("3");
        }

        private void btn4_Click(object sender, EventArgs e)
        {
            addNum("4");
        }

        private void btn5_Click(object sender, EventArgs e)
        {
            addNum("5");
        }

        private void btn6_Click(object sender, EventArgs e)
        {
            addNum("6");
        }

        private void btn7_Click(object sender, EventArgs e)
        {
            addNum("7");
        }

        private void btn8_Click(object sender, EventArgs e)
        {
            addNum("8");
        }

        private void btn9_Click(object sender, EventArgs e)
        {
            addNum("9");
        }

        private void btn0_Click(object sender, EventArgs e)
        {
            addNum("0");
        }

        private void addOperand(string operand)
        {
            if (txtpantalla.Text != "" && !txtpantalla.Text.Contains("="))
            {
                string[] nums = txtpantalla.Text.Split('+', '-', '*', '/');
                if (nums.Length < 4)
                {
                    int l = txtpantalla.Text.Length;
                    l--;
                    string c = txtpantalla.Text.Substring(l, 1);
                    if (c == "+" || c == "-" || c == "*" || c == "/" || c == ".")
                    {
                        txtpantalla.Text = txtpantalla.Text.Substring(0, l) + operand;
                    }
                    else
                        txtpantalla.Text = txtpantalla.Text + operand;
                }
            }
        }

        private void btnsum_Click(object sender, EventArgs e)
        {
            addOperand("+");
        }

        private void btnsub_Click(object sender, EventArgs e)
        {
            addOperand("-");
        }

        private void btnmul_Click(object sender, EventArgs e)
        {
            addOperand("*");
        }

        private void btndiv_Click(object sender, EventArgs e)
        {
            addOperand("/");
        }

        private void btnback_Click(object sender, EventArgs e)
        {
            if (txtpantalla.Text.Contains("="))
                txtpantalla.Text = "";
            int l = txtpantalla.Text.Length;
            if (l > 0)
            {
                l--;
                txtpantalla.Text = txtpantalla.Text.Substring(0, l);
            }
        }

        private void btnclean_Click(object sender, EventArgs e)
        {
            txtpantalla.Text ="";
        }

        private void btnequ_Click(object sender, EventArgs e)
        {
            if (!txtpantalla.Text.Contains("=") && txtpantalla.Text != "" && txtpantalla.Text.Split('+','-','*','/').Length > 1)
            {
                Validar(txtpantalla.Text);
            }
        }

        private void Validar(string strExpr)
        {
            txtpantalla.Text = functions.Evaluar(strExpr).ToString();
        }

        private void btnpoint_Click(object sender, EventArgs e)
        {
            if (txtpantalla.Text.Contains("="))
                txtpantalla.Text = "";
            int l = txtpantalla.Text.Length;
            if (l != 0)
            {
                l--;
                string c = txtpantalla.Text.Substring(l, 1);
                if (c == "+" || c == "-" || c == "*" || c == "/")
                    txtpantalla.Text = txtpantalla.Text + "0.";
                else if (c != ".")
                {
                    string[] nums = txtpantalla.Text.Split('+', '-', '*', '/');
                    if (!nums[nums.Length - 1].Contains("."))
                        txtpantalla.Text = txtpantalla.Text + ".";
                }
            }
            else
                txtpantalla.Text = txtpantalla.Text.Substring(0, l) + "0.";
        }

    }
}
