﻿namespace Calculator
{
    partial class Calc
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtpantalla = new System.Windows.Forms.TextBox();
            this.btn1 = new System.Windows.Forms.Button();
            this.btn2 = new System.Windows.Forms.Button();
            this.btn3 = new System.Windows.Forms.Button();
            this.btn4 = new System.Windows.Forms.Button();
            this.btn5 = new System.Windows.Forms.Button();
            this.btn6 = new System.Windows.Forms.Button();
            this.btn7 = new System.Windows.Forms.Button();
            this.btn8 = new System.Windows.Forms.Button();
            this.btn9 = new System.Windows.Forms.Button();
            this.btn0 = new System.Windows.Forms.Button();
            this.btnsum = new System.Windows.Forms.Button();
            this.btnsub = new System.Windows.Forms.Button();
            this.btnmul = new System.Windows.Forms.Button();
            this.btndiv = new System.Windows.Forms.Button();
            this.btnback = new System.Windows.Forms.Button();
            this.btnclean = new System.Windows.Forms.Button();
            this.btnequ = new System.Windows.Forms.Button();
            this.btnpoint = new System.Windows.Forms.Button();
            this.logo = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.logo)).BeginInit();
            this.SuspendLayout();
            // 
            // txtpantalla
            // 
            this.txtpantalla.Enabled = false;
            this.txtpantalla.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtpantalla.ForeColor = System.Drawing.Color.Blue;
            this.txtpantalla.Location = new System.Drawing.Point(2, 61);
            this.txtpantalla.Name = "txtpantalla";
            this.txtpantalla.Size = new System.Drawing.Size(496, 22);
            this.txtpantalla.TabIndex = 0;
            this.txtpantalla.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // btn1
            // 
            this.btn1.BackColor = System.Drawing.Color.Blue;
            this.btn1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn1.ForeColor = System.Drawing.Color.Yellow;
            this.btn1.Location = new System.Drawing.Point(2, 91);
            this.btn1.Name = "btn1";
            this.btn1.Size = new System.Drawing.Size(96, 28);
            this.btn1.TabIndex = 1;
            this.btn1.Text = "1";
            this.btn1.UseVisualStyleBackColor = false;
            this.btn1.Click += new System.EventHandler(this.btn1_Click);
            // 
            // btn2
            // 
            this.btn2.BackColor = System.Drawing.Color.Blue;
            this.btn2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn2.ForeColor = System.Drawing.Color.Yellow;
            this.btn2.Location = new System.Drawing.Point(102, 91);
            this.btn2.Name = "btn2";
            this.btn2.Size = new System.Drawing.Size(96, 28);
            this.btn2.TabIndex = 2;
            this.btn2.Text = "2";
            this.btn2.UseVisualStyleBackColor = false;
            this.btn2.Click += new System.EventHandler(this.btn2_Click);
            // 
            // btn3
            // 
            this.btn3.BackColor = System.Drawing.Color.Blue;
            this.btn3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn3.ForeColor = System.Drawing.Color.Yellow;
            this.btn3.Location = new System.Drawing.Point(202, 91);
            this.btn3.Name = "btn3";
            this.btn3.Size = new System.Drawing.Size(96, 28);
            this.btn3.TabIndex = 3;
            this.btn3.Text = "3";
            this.btn3.UseVisualStyleBackColor = false;
            this.btn3.Click += new System.EventHandler(this.btn3_Click);
            // 
            // btn4
            // 
            this.btn4.BackColor = System.Drawing.Color.Blue;
            this.btn4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn4.ForeColor = System.Drawing.Color.Yellow;
            this.btn4.Location = new System.Drawing.Point(2, 121);
            this.btn4.Name = "btn4";
            this.btn4.Size = new System.Drawing.Size(96, 28);
            this.btn4.TabIndex = 4;
            this.btn4.Text = "4";
            this.btn4.UseVisualStyleBackColor = false;
            this.btn4.Click += new System.EventHandler(this.btn4_Click);
            // 
            // btn5
            // 
            this.btn5.BackColor = System.Drawing.Color.Blue;
            this.btn5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn5.ForeColor = System.Drawing.Color.Yellow;
            this.btn5.Location = new System.Drawing.Point(102, 121);
            this.btn5.Name = "btn5";
            this.btn5.Size = new System.Drawing.Size(96, 28);
            this.btn5.TabIndex = 5;
            this.btn5.Text = "5";
            this.btn5.UseVisualStyleBackColor = false;
            this.btn5.Click += new System.EventHandler(this.btn5_Click);
            // 
            // btn6
            // 
            this.btn6.BackColor = System.Drawing.Color.Blue;
            this.btn6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn6.ForeColor = System.Drawing.Color.Yellow;
            this.btn6.Location = new System.Drawing.Point(202, 121);
            this.btn6.Name = "btn6";
            this.btn6.Size = new System.Drawing.Size(96, 28);
            this.btn6.TabIndex = 6;
            this.btn6.Text = "6";
            this.btn6.UseVisualStyleBackColor = false;
            this.btn6.Click += new System.EventHandler(this.btn6_Click);
            // 
            // btn7
            // 
            this.btn7.BackColor = System.Drawing.Color.Blue;
            this.btn7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn7.ForeColor = System.Drawing.Color.Yellow;
            this.btn7.Location = new System.Drawing.Point(2, 151);
            this.btn7.Name = "btn7";
            this.btn7.Size = new System.Drawing.Size(96, 28);
            this.btn7.TabIndex = 7;
            this.btn7.Text = "7";
            this.btn7.UseVisualStyleBackColor = false;
            this.btn7.Click += new System.EventHandler(this.btn7_Click);
            // 
            // btn8
            // 
            this.btn8.BackColor = System.Drawing.Color.Blue;
            this.btn8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn8.ForeColor = System.Drawing.Color.Yellow;
            this.btn8.Location = new System.Drawing.Point(102, 151);
            this.btn8.Name = "btn8";
            this.btn8.Size = new System.Drawing.Size(96, 28);
            this.btn8.TabIndex = 8;
            this.btn8.Text = "8";
            this.btn8.UseVisualStyleBackColor = false;
            this.btn8.Click += new System.EventHandler(this.btn8_Click);
            // 
            // btn9
            // 
            this.btn9.BackColor = System.Drawing.Color.Blue;
            this.btn9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn9.ForeColor = System.Drawing.Color.Yellow;
            this.btn9.Location = new System.Drawing.Point(202, 151);
            this.btn9.Name = "btn9";
            this.btn9.Size = new System.Drawing.Size(96, 28);
            this.btn9.TabIndex = 9;
            this.btn9.Text = "9";
            this.btn9.UseVisualStyleBackColor = false;
            this.btn9.Click += new System.EventHandler(this.btn9_Click);
            // 
            // btn0
            // 
            this.btn0.BackColor = System.Drawing.Color.Blue;
            this.btn0.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn0.ForeColor = System.Drawing.Color.Yellow;
            this.btn0.Location = new System.Drawing.Point(2, 181);
            this.btn0.Name = "btn0";
            this.btn0.Size = new System.Drawing.Size(196, 28);
            this.btn0.TabIndex = 10;
            this.btn0.Text = "0";
            this.btn0.UseVisualStyleBackColor = false;
            this.btn0.Click += new System.EventHandler(this.btn0_Click);
            // 
            // btnsum
            // 
            this.btnsum.BackColor = System.Drawing.Color.Blue;
            this.btnsum.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnsum.ForeColor = System.Drawing.Color.Yellow;
            this.btnsum.Location = new System.Drawing.Point(302, 91);
            this.btnsum.Name = "btnsum";
            this.btnsum.Size = new System.Drawing.Size(96, 28);
            this.btnsum.TabIndex = 12;
            this.btnsum.Text = "+";
            this.btnsum.UseVisualStyleBackColor = false;
            this.btnsum.Click += new System.EventHandler(this.btnsum_Click);
            // 
            // btnsub
            // 
            this.btnsub.BackColor = System.Drawing.Color.Blue;
            this.btnsub.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnsub.ForeColor = System.Drawing.Color.Yellow;
            this.btnsub.Location = new System.Drawing.Point(302, 121);
            this.btnsub.Name = "btnsub";
            this.btnsub.Size = new System.Drawing.Size(96, 28);
            this.btnsub.TabIndex = 13;
            this.btnsub.Text = "-";
            this.btnsub.UseVisualStyleBackColor = false;
            this.btnsub.Click += new System.EventHandler(this.btnsub_Click);
            // 
            // btnmul
            // 
            this.btnmul.BackColor = System.Drawing.Color.Blue;
            this.btnmul.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnmul.ForeColor = System.Drawing.Color.Yellow;
            this.btnmul.Location = new System.Drawing.Point(302, 151);
            this.btnmul.Name = "btnmul";
            this.btnmul.Size = new System.Drawing.Size(96, 28);
            this.btnmul.TabIndex = 14;
            this.btnmul.Text = "*";
            this.btnmul.UseVisualStyleBackColor = false;
            this.btnmul.Click += new System.EventHandler(this.btnmul_Click);
            // 
            // btndiv
            // 
            this.btndiv.BackColor = System.Drawing.Color.Blue;
            this.btndiv.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btndiv.ForeColor = System.Drawing.Color.Yellow;
            this.btndiv.Location = new System.Drawing.Point(302, 181);
            this.btndiv.Name = "btndiv";
            this.btndiv.Size = new System.Drawing.Size(96, 28);
            this.btndiv.TabIndex = 15;
            this.btndiv.Text = "/";
            this.btndiv.UseVisualStyleBackColor = false;
            this.btndiv.Click += new System.EventHandler(this.btndiv_Click);
            // 
            // btnback
            // 
            this.btnback.BackColor = System.Drawing.Color.Blue;
            this.btnback.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnback.ForeColor = System.Drawing.Color.Yellow;
            this.btnback.Location = new System.Drawing.Point(402, 91);
            this.btnback.Name = "btnback";
            this.btnback.Size = new System.Drawing.Size(96, 28);
            this.btnback.TabIndex = 17;
            this.btnback.Text = "←";
            this.btnback.UseVisualStyleBackColor = false;
            this.btnback.Click += new System.EventHandler(this.btnback_Click);
            // 
            // btnclean
            // 
            this.btnclean.BackColor = System.Drawing.Color.Blue;
            this.btnclean.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnclean.ForeColor = System.Drawing.Color.Yellow;
            this.btnclean.Location = new System.Drawing.Point(402, 121);
            this.btnclean.Name = "btnclean";
            this.btnclean.Size = new System.Drawing.Size(96, 28);
            this.btnclean.TabIndex = 18;
            this.btnclean.Text = "C";
            this.btnclean.UseVisualStyleBackColor = false;
            this.btnclean.Click += new System.EventHandler(this.btnclean_Click);
            // 
            // btnequ
            // 
            this.btnequ.BackColor = System.Drawing.Color.Blue;
            this.btnequ.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnequ.ForeColor = System.Drawing.Color.Yellow;
            this.btnequ.Location = new System.Drawing.Point(402, 151);
            this.btnequ.Name = "btnequ";
            this.btnequ.Size = new System.Drawing.Size(96, 58);
            this.btnequ.TabIndex = 16;
            this.btnequ.Text = "=";
            this.btnequ.UseVisualStyleBackColor = false;
            this.btnequ.Click += new System.EventHandler(this.btnequ_Click);
            // 
            // btnpoint
            // 
            this.btnpoint.BackColor = System.Drawing.Color.Blue;
            this.btnpoint.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnpoint.ForeColor = System.Drawing.Color.Yellow;
            this.btnpoint.Location = new System.Drawing.Point(202, 181);
            this.btnpoint.Name = "btnpoint";
            this.btnpoint.Size = new System.Drawing.Size(96, 28);
            this.btnpoint.TabIndex = 11;
            this.btnpoint.Text = ".";
            this.btnpoint.UseVisualStyleBackColor = false;
            this.btnpoint.Click += new System.EventHandler(this.btnpoint_Click);
            // 
            // logo
            // 
            this.logo.ImageLocation = "https://upload.wikimedia.org/wikipedia/commons/thumb/c/ce/Diebold_Nixdorf_logo_20" +
    "18.jpg/220px-Diebold_Nixdorf_logo_2018.jpg";
            this.logo.Location = new System.Drawing.Point(225, 5);
            this.logo.Name = "logo";
            this.logo.Size = new System.Drawing.Size(50, 50);
            this.logo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.logo.TabIndex = 0;
            this.logo.TabStop = false;
            // 
            // Calc
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Orange;
            this.ClientSize = new System.Drawing.Size(500, 210);
            this.Controls.Add(this.logo);
            this.Controls.Add(this.txtpantalla);
            this.Controls.Add(this.btn1);
            this.Controls.Add(this.btn2);
            this.Controls.Add(this.btn3);
            this.Controls.Add(this.btn4);
            this.Controls.Add(this.btn5);
            this.Controls.Add(this.btn6);
            this.Controls.Add(this.btn7);
            this.Controls.Add(this.btn8);
            this.Controls.Add(this.btn9);
            this.Controls.Add(this.btn0);
            this.Controls.Add(this.btnsum);
            this.Controls.Add(this.btnsub);
            this.Controls.Add(this.btnmul);
            this.Controls.Add(this.btndiv);
            this.Controls.Add(this.btnback);
            this.Controls.Add(this.btnclean);
            this.Controls.Add(this.btnequ);
            this.Controls.Add(this.btnpoint);
            this.Name = "Calc";
            this.Text = "DN Calculadora";
            ((System.ComponentModel.ISupportInitialize)(this.logo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtpantalla;
        private System.Windows.Forms.Button btn1;
        private System.Windows.Forms.Button btn2;
        private System.Windows.Forms.Button btn3;
        private System.Windows.Forms.Button btn4;
        private System.Windows.Forms.Button btn5;
        private System.Windows.Forms.Button btn6;
        private System.Windows.Forms.Button btn7;
        private System.Windows.Forms.Button btn8;
        private System.Windows.Forms.Button btn9;
        private System.Windows.Forms.Button btn0;
        private System.Windows.Forms.Button btnpoint;
        private System.Windows.Forms.Button btnsum;
        private System.Windows.Forms.Button btnsub;
        private System.Windows.Forms.Button btnmul;
        private System.Windows.Forms.Button btndiv;
        private System.Windows.Forms.Button btnback;
        private System.Windows.Forms.Button btnclean;
        private System.Windows.Forms.Button btnequ;
        private System.Windows.Forms.PictureBox logo;
    }
}

