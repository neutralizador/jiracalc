using System;

namespace Calculator
{
	internal class controllerDiv
	{
		public double div(double n1, double n2)
		{
			return(n1/n2);
		}

        public void writeResult(double input)
        {
            Console.WriteLine("El resultado es:{0}\n", input);
        }

        public void doDiv(double n1, double n2)
    {
      double result = div(n1, n2);
      if(result == double.PositiveInfinity
      || result == double.NegativeInfinity)
      {
        Console.WriteLine("No es posible la división entre cero.");
      }
      else
      {
        writeResult(result);
      }
    }
	}
}