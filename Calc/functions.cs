﻿using System;
using System.Data;

namespace Calculator
{
    public class functions
    {
        public static double Evaluar(string expression)
        {
            DataTable table = new DataTable();
            table.Columns.Add("expression", typeof(string), expression);
            DataRow row = table.NewRow();
            table.Rows.Add(row);
            //txtpantalla.Text = strExpr + "=" + (string)row["expression"];
            return double.Parse((string)row["expression"]);
        }

    }
}
